package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;


public class MyTriplicate<T extends Comparable<T>> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        //Sorting the list
        Collections.sort(list);
        //Getting the size of the list
        int n = list.size();

        for (int i = 2; i < n; i++) {
            //Checking if the element at index i is equal to the element at index i+2
            if (list.get(i).equals(list.get(i-2))) {
                return list.get(i);
            }
        }
        return null;
    }
    
}
